module goftp.io/server

go 1.12

require (
	code.gitea.io/log v0.0.0-20191208183219-f31613838113
	github.com/jlaffaye/ftp v0.0.0-20190624084859-c1312a7102bf
	github.com/minio/minio-go/v6 v6.0.46
	github.com/stretchr/testify v1.3.0
	gopkg.in/src-d/go-git.v4 v4.13.1
)
